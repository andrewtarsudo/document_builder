# Роли пользователей
:roles: Roles

Роли в системе:

* <<admin,Администратор>>;
* <<user,Пользователь>>;
* {roles};

[[admin]]
## Администратор

* #Просмотр шаблонов#;
* Создание шаблонов;
* Изменение шаблонов;
* Удаление шаблонов;
* Переопределение шаблонов для проекта;
* Добавление форматов документа;
* Изменение форматов документа;
* Генерация документа;
* Просмотр изменений всех пользователей и администраторов;
* Отмена изменений пользователей;

[[user]]
## Пользователь

* Просмотр шаблонов;
* Создание шаблонов;
* Генерация документа;
* Просмотр изменений всех пользователей;

NOTE: Note

### Code

```yaml
name: TemplateName
types:
- DocumentType
- DocumentType
products:
- ProductName
- ProductName
formats:
- DocumentFormat
- DocumentFormat

structure:
- path_to_dir
- path_to_dir

include:
- path_to_dir:
  before: path_to_file
- path_to_file:
  before: path_to_file
- path_to_dir:
  before: path_to_file

exclude:
- pattern
- pattern
```

include::Constr_doc.adoc\[]